package eu.paasage.upperware.profiler.rp.util;

public class UnavailableModelException extends Exception {

	/** */
	private static final long serialVersionUID = -9058158922515372750L;

	public UnavailableModelException(String error) {
		super(error);
	}

}
